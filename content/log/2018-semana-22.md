+++
title = "Semana 22 del año 2018"
author = ["drymer"]
publishDate = 2018-06-03T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [DevSecOps - A New Chance for Security](https://dzone.com/articles/shifting-left-devsecops): tiene que ver con devops, seguridad y se puede leer en un{os} 13 minuto{s}
-   [Sobre la amistad entre mujeres](https://genericidios.wordpress.com/2018/05/27/sobre-la-amistad-entre-mujeres/): tiene que ver con feminismo y se puede leer en un{os} 8 minuto{s}
-   [Detención o libertad. El concepto “retención” es ilegal](https://www.lamarea.com/2018/05/28/detencion-o-libertad-el-concepto-retencion-es-ilegal/): tiene que ver con activismo y se puede leer en un{os} 9 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Application Performance Troubleshooting Expertise: 9 New Microservices Issues Part 6 - Instana](https://www.instana.com/blog/application-performance-troubleshooting-expertise/): tiene que ver con microservicios y se puede leer en un{os} 5 minuto{s}
-   [Top 5 Ways to Tame Kubernetes Complexity](https://dzone.com/articles/top-5-ways-to-tame-kubernetes-complexity): tiene que ver con kubernetes, microservicios y se puede leer en un{os} 2 minuto{s}
-   [cloudsploit/scans](https://github.com/cloudsploit/scans): tiene que ver con aws y se puede leer en un{os} 8 minuto{s}
-   [neuvector/kubernetes-cis-benchmark](https://github.com/neuvector/kubernetes-cis-benchmark): tiene que ver con kubernetes y se puede leer en un{os} 0 minuto{s}
-   [Emacs 26 Brings Generators and Threads « null program](http://nullprogram.com/blog/2018/05/31/): tiene que ver con emacs, nada en general y se puede leer en un{os} 11 minuto{s}
-   [kubernetes/test-infra](https://github.com/kubernetes/test-infra): tiene que ver con kubernetes y se puede leer en un{os} 4 minuto{s}
-   [Service Catalog - Kubernetes](https://kubernetes.io/docs/concepts/extend-kubernetes/service-catalog/): tiene que ver con kubernetes y se puede leer en un{os} 8 minuto{s}
-   [logpad.el: Notepad-like logging for GNU Emacs](https://www.reddit.com/r/emacs/comments/8ns40r/logpadel%5Fnotepadlike%5Flogging%5Ffor%5Fgnu%5Femacs/): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Emacs Notes A finger-typer makes a horrible Emacs user: Learn to touch type with \`Typespeed\`](https://www.reddit.com/r/emacs/comments/8nspas/emacs%5Fnotes%5Fa%5Ffingertyper%5Fmakes%5Fa%5Fhorrible%5Femacs/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [A fancy Emacs version](http://manuel-uberti.github.io//emacs/2018/05/25/display-version/): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   kubesec.io]]: tiene que ver con kubernetes y se puede leer en un{os} 1 minuto{s}
-   [asobti/kube-monkey](https://github.com/asobti/kube-monkey): tiene que ver con kubernetes y se puede leer en un{os} 6 minuto{s}
-   [johanhaleby/kubetail](https://github.com/johanhaleby/kubetail): tiene que ver con kubernetes y se puede leer en un{os} 2 minuto{s}
-   [How to Become a Successful Technical Speaker  Hacker Noon](https://hackernoon.com/how-to-become-a-successful-technical-speaker-4685fe1be335?source=rss----3a8144eabfe3---4&gi=98102d9bd769): tiene que ver con productividad y se puede leer en un{os} 11 minuto{s}
-   [Gestión de tiempo y GTD](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=2T%5FYNXF5CA4&format=xml): tiene que ver con productividad y se puede leer en un{os} 0 minuto{s}
-   [thblt/org-babel-tangle.py](https://github.com/thblt/org-babel-tangle.py): tiene que ver con org-mode y se puede leer en un{os} 0 minuto{s}
-   [How To Quickly Diagnose Your Time Management Problem](https://healthyleader.health/diagnose-your-time-management-problem/): tiene que ver con productividad y se puede leer en un{os} 25 minuto{s}
-   [La generación de los hipersensibles](https://www.semana.com/vida-moderna/articulo/los-jovenes-de-hoy-son-hipersensibles/552930): tiene que ver con cuidados y se puede leer en un{os} 0 minuto{s}
-   [Should that be a Microservice? Keep These Six Factors in Mind](https://content.pivotal.io/blog/should-that-be-a-microservice-keep-these-six-factors-in-mind): tiene que ver con microservicios y se puede leer en un{os} 8 minuto{s}
