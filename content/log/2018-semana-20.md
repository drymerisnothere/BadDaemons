+++
title = "Semana 20 del año 2018"
author = ["drymer"]
publishDate = 2018-05-20T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Monitoring Using Spring Boot 2.0, Prometheus and Grafana (Part 2 — Exposing Metrics)](https://dzone.com/articles/monitoring-using-spring-boot-2-prometheus-and-graf): tiene que ver con microservicios y se puede leer en un{os} 2 minuto{s}
-   [Hard Multi-Tenancy in Kubernetes](https://blog.jessfraz.com/post/hard-multi-tenancy-in-kubernetes/): tiene que ver con kubernetes y se puede leer en un{os} 9 minuto{s}
-   [Kubernetes Chaos Engineering: Lessons Learned](https://learnk8s.io/blog/kubernetes-chaos-engineering-lessons-learned): tiene que ver con kubernetes y se puede leer en un{os} 11 minuto{s}
-   [En el Día contra la LGTBfobia, aprende a abrir la boca sin ofender a las personas trans](https://www.elsaltodiario.com/transexualidad/dia-contra-lgtbfobia-transfobia-lenguaje-personas-trans): tiene que ver con cuidados y se puede leer en un{os} 6 minuto{s}
-   [Tarballs, the ultimate container image format — 2018 — Blog — GuixSD](https://www.gnu.org/software/guix/blog/2018/tarballs-the-ultimate-container-image-format/): tiene que ver con contenedores y se puede leer en un{os} 6 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Microservices: Externalized Configuration](https://dzone.com/articles/microservices-externalized-configuration): tiene que ver con microservicios y se puede leer en un{os} 5 minuto{s}
-   [Service Mesh vs API Gateway  Microservices in Practice](https://medium.com/microservices-in-practice/service-mesh-vs-api-gateway-a6d814b9bf56): tiene que ver con microservicios y se puede leer en un{os} 2 minuto{s}
-   [The Emacs Web Wowser: Browsing and Searching the Web with Emacs](https://emacsnotes.wordpress.com/2018/05/19/the-emacs-web-wowser-browsing-and-searching-the-web-with-emacs/): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [r/emacs - org-mode extensions? Workflows?](https://www.reddit.com/r/emacs/comments/8jy7p1/orgmode%5Fextensions%5Fworkflows/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Kubernetes Co-founder talks about new challenges](https://www.reddit.com/r/linux/comments/8ki6o4/kubernetes%5Fcofounder%5Ftalks%5Fabout%5Fnew%5Fchallenges/): tiene que ver con kubernetes y se puede leer en un{os} 2 minuto{s}
-   [Kubernetes Namespaces Explained - DZone Cloud](https://dzone.com/articles/kubernetes-namespaces-explained): tiene que ver con kubernetes y se puede leer en un{os} 7 minuto{s}
-   [Microservices Traffic Management Using Istio on Kubernetes](https://dzone.com/articles/microservices-traffic-management-using-istio-on-ku): tiene que ver con kubernetes, microservicios y se puede leer en un{os} 10 minuto{s}
-   [Shell Scripting and Security](https://www.reddit.com/r/linux/comments/8ku100/shell%5Fscripting%5Fand%5Fsecurity/): tiene que ver con bash, seguridad y se puede leer en un{os} 2 minuto{s}
-   [Five Simple Steps to Becoming an International™ Conference Speaker!](https://hackernoon.com/five-simple-steps-to-becoming-an-international-conference-speaker-a4d13c02529?source=rss----3a8144eabfe3---4&gi=921f10d90666): tiene que ver con productividad y se puede leer en un{os} 8 minuto{s}
-   [10 Firefox Dev Tools tricks that will blow your mind](https://hackernoon.com/10-firefox-dev-tools-tricks-that-will-blow-your-mind-d641f1714903?source=rss----3a8144eabfe3---4&gi=4fe0205c24ce): tiene que ver con desarrollo y se puede leer en un{os} 2 minuto{s}
-   [Chaos Engineering: Managing Complexity by Breaking Things - DZone Performance](https://dzone.com/articles/chaos-engineering-managing-complexity-by-breaking): tiene que ver con chaos y se puede leer en un{os} 7 minuto{s}
-   [Meta Productivity -- How do you go about finding the productivity tools and strategies that work best for you?](https://www.reddit.com/r/productivity/comments/8jz41x/meta%5Fproductivity%5Fhow%5Fdo%5Fyou%5Fgo%5Fabout%5Ffinding%5Fthe/): tiene que ver con productividad y se puede leer en un{os} 4 minuto{s}
-   [Spring Security With Spring Boot 2.0: UserDetailsService](https://dzone.com/articles/spring-security-with-spring-boot-20-userdetailsser): tiene que ver con microservicios y se puede leer en un{os} 1 minuto{s}
-   [Kubernetes as a Service: Implementing KaaS](https://dzone.com/articles/kubernetes-as-a-service-implementing-kaas): tiene que ver con kubernetes, devops y se puede leer en un{os} 5 minuto{s}
