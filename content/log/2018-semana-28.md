+++
title = "Semana 28 del año 2018"
author = ["drymer"]
publishDate = 2018-07-15T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Dangerous Python Functions, Part 1](https://www.kevinlondon.com/2015/07/26/dangerous-python-functions.html): tiene que ver con python y se puede leer en un{os} 3 minuto{s}
-   [Principles of Container-based Application Design - Kubernetes](https://kubernetes.io/blog/2018/03/principles-of-container-app-design/): tiene que ver con kubernetes y se puede leer en un{os} 2 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [DevSecOps: Dreams, Teams, and Architecture](https://dzone.com/articles/devsecops-dreams-teams-and-architecture): tiene que ver con devops y se puede leer en un{os} 3 minuto{s}
-   [RFID Thief v2.0](https://scund00r.com/all/rfid/tutorial/2018/07/12/rfid-theif-v2.html): tiene que ver con seguridad y se puede leer en un{os} 7 minuto{s}
-   [Hasta cuatro años de cárcel si una ciudadana golpea a un policía. Cuatro días sin sueldo si es al revés](https://www.eldiario.es/cv/Carcel-agredido-abofeteo-Valencia-sancionado%5F0%5F792321138.html): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Mijaíl Bakunin: El Poder Corrompe a los Mejores (1867)](http://rebeldealegre.blogspot.com/2015/03/mijail-bakunin-el-poder-corrompe-los.html): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Liberapay is in trouble  Liberapay’s Blog](https://medium.com/liberapay-blog/liberapay-is-in-trouble-b58b40714d82): tiene que ver con redes libres y se puede leer en un{os} 4 minuto{s}
-   [#BugBounty — Compromising User Account- ”How I was able to compromise user account via HTTP…](https://medium.com/@logicbomb%5F1/bugbounty-compromising-user-account-how-i-was-able-to-compromise-user-account-via-http-4288068b901f): tiene que ver con seguridad y se puede leer en un{os} 3 minuto{s}
-   [How to drop 10 million packets per second](https://blog.cloudflare.com/how-to-drop-10-million-packets/): tiene que ver con sysadmin y se puede leer en un{os} 13 minuto{s}
-   [¿Por qué los hombres nos seguimos comportando como “hombres”?](http://www.pikaramagazine.com/2018/07/por-que-los-hombres-nos-seguimos-comportando-como-hombres/): tiene que ver con feminismo y se puede leer en un{os} 9 minuto{s}
-   [Airflow on Kubernetes (Part 1): A Different Kind of Operator - Kubernetes](https://kubernetes.io/blog/2018/06/28/airflow-on-kubernetes-part-1-a-different-kind-of-operator/): tiene que ver con kubernetes y se puede leer en un{os} 8 minuto{s}
-   [r/linux - Guido van Rossum is stepping down as Python's Benevolent Dictator for Life](https://www.reddit.com/r/linux/comments/8ybiar/guido%5Fvan%5Frossum%5Fis%5Fstepping%5Fdown%5Fas%5Fpythons/): tiene que ver con python y se puede leer en un{os} 0 minuto{s}
-   [Zero-downtime Deployment in Kubernetes with Jenkins - Kubernetes](https://kubernetes.io/blog/2018/04/30/zero-downtime-deployment-kubernetes-jenkins/): tiene que ver con kubernetes y se puede leer en un{os} 9 minuto{s}
-   [Developing on Kubernetes - Kubernetes](https://kubernetes.io/blog/2018/05/01/developing-on-kubernetes/): tiene que ver con kubernetes y se puede leer en un{os} 16 minuto{s}
-   [CoreDNS GA for Kubernetes Cluster DNS - Kubernetes](https://kubernetes.io/blog/2018/07/10/coredns-ga-for-kubernetes-cluster-dns/): tiene que ver con kubernetes y se puede leer en un{os} 9 minuto{s}
-   [Dynamic Kubelet Configuration - Kubernetes](https://kubernetes.io/blog/2018/07/11/dynamic-kubelet-configuration/): tiene que ver con kubernetes y se puede leer en un{os} 3 minuto{s}
