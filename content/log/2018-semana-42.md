+++
title = "Semana 42 del año 2018"
author = ["drymer"]
publishDate = 2018-10-21T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [GitLab + STEM Gems: Giving girls role models in tech](https://about.gitlab.com/2018/10/08/stem-gems-give-girls-role-models/): tiene que ver con git, feminismo y se puede leer un{os} 14 minuto{s}
-   [Draw.io for threat modeling | michenriksen.com](http://michenriksen.com/blog/drawio-for-threat-modeling/): tiene que ver con seguridad y se puede leer un{os} 3 minuto{s}
-   [5G Got me Fired  John C. Dvorak  Medium](https://medium.com/@dvorak/5g-got-me-fired-ce407e584c4a): tiene que ver con nada en general y se puede leer un{os} 2 minuto{s}
-   [Radiografía del Madrid del futuro desde el presente: ‘Smart City’ significa ‘ciudad privada’](https://www.elsaltodiario.com/ciudad-marca/radiografia-madrid-futuro-smart-city-ciudad-privada): tiene que ver con nada en general y se puede leer un{os} 26 minuto{s}
-   [Reproducible Research and Literate Programming in Econometrics](http://irreal.org/blog/?p=7519): tiene que ver con programacion y se puede leer un{os} 1 minuto{s}
-   [A Brief History of Anarchism: The European Tradition](http://content.time.com/time/world/article/0,8599,2040304,00.html): tiene que ver con anarquismo y se puede leer un{os} 5 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Di NO a la meritocracia. ¿Cómo componemos nuestro Development Team?](https://mamaqueesscrum.com/2018/10/08/di-no-a-la-meritocracia-como-componemos-nuestro-development-team/): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [Another Take on Going Paperless](http://irreal.org/blog/?p=7526): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Your IQ Matters Less Than You Think - Issue 65: In Plain Sight - Nautilus](http://nautil.us/issue/65/in-plain-sight/your-iq-matters-less-than-you-think): tiene que ver con nada en general y se puede leer en un{os} 15 minuto{s}
-   [Introducing the Non-Code Contributor’s Guide](https://kubernetes.io/blog/2018/10/04/introducing-the-non-code-contributors-guide/): tiene que ver con nada en general y se puede leer en un{os} 3 minuto{s}
-   [r/orgmode - Fully Reproducible Research Paper Export Function](https://www.reddit.com/r/orgmode/comments/9j8wts/fully%5Freproducible%5Fresearch%5Fpaper%5Fexport%5Ffunction/): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [Declarative application management in Kubernetes](https://docs.google.com/document/d/1cLPGweVEYrVqQvBLJg6sxV-TrE5Rm2MNOBA%5FcxZP2WU/edit?%5Fescaped%5Ffragment%5F=): tiene que ver con kubernetes y se puede leer en un{os} 42 minuto{s}
