+++
title = "Semana 16 del año 2019"
author = ["drymer"]
publishDate = 2019-04-28T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Faking fires: Get better incident management with practise](https://medium.com/kudos-engineering/faking-fires-get-better-incident-management-with-practise-e61a5d66578d): tiene que ver con sysadmin y se puede leer un{os} 6 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [A guide to giving & receiving effective feedback](https://dev.to/kelly/a-guide-to-giving-receiving-effective-feedback-2h4p): tiene que ver con la vida y se puede leer en un{os} 3 minuto{s}
-   [Unit Testing Anti-Patterns, Full List](https://www.yegor256.com/2018/12/11/unit-testing-anti-patterns.html): tiene que ver con antipatrones y se puede leer en un{os} 5 minuto{s}
-   [r/vim - Why Vi Rocks](https://www.reddit.com/r/vim/comments/bbueis/why%5Fvi%5Frocks/): tiene que ver con vim y se puede leer en un{os} 0 minuto{s}
-   [How to Get Into SRE](https://blog.alicegoldfuss.com/how-to-get-into-sre/): tiene que ver con sre y se puede leer en un{os} 19 minuto{s}
-   [Container Design Patterns for Kubernetes](https://www.weave.works/blog/container-design-patterns-for-kubernetes/): tiene que ver con kubernetes y se puede leer en un{os} 6 minuto{s}
