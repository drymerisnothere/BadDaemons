+++
title = "Semana 44 del año 2018"
author = ["drymer"]
publishDate = 2018-11-04T00:00:00+01:00
draft = false
+++

## Articulos leídos {#articulos-leídos}

-   [¿Por qué se me cuestiona que escriba sobre la violencia de género?](https://confilegal.com/20181031-por-que-se-me-cuestiona-que-escriba-sobre-la-violencia-de-genero/): tiene que ver con feminismo y se puede leer en un{os} 3 minuto{s}
-   [Mil maneras de ser personas trans](https://www.elsaltodiario.com/libertades/mil-maneras-ser-trans): tiene que ver con feminismo y se puede leer en un{os} 5 minuto{s}
-   [Feminismo, trileros y caballos de Troya](http://www.pikaramagazine.com/2018/10/feminismo-trileros-y-caballos-de-troya/): tiene que ver con feminismo y se puede leer en un{os} 10 minuto{s}
-   [Los jueces y la ciencia caminan separados en el caso de enfermos hipersensibles](https://www.eldiario.es/sociedad/sensibilidad%5Fquimica%5Fmultiple-electrosensibilidad-ciencia%5F0%5F829067354.html): tiene que ver con salud y se puede leer en un{os} 6 minuto{s}
-   [Why I've finally had it with my Linux server and I'm moving back to Windows](https://www.zdnet.com/article/why-ive-finally-had-it-with-my-linux-server-and-im-moving-back-to-windows/): tiene que ver con linux y se puede leer en un{os} 6 minuto{s}
-   [I am not diversity](https://hackernoon.com/i-am-not-diversity-8f5abc876acc?gi=49850b7a4c7a): tiene que ver con racismo y se puede leer en un{os} 5 minuto{s}
-   [Essential HTTP Headers for Securing Your Web Server](https://pentest-tools.com/blog/essential-http-security-headers/): tiene que ver con http y se puede leer en un{os} 9 minuto{s}
