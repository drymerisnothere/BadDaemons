+++
title = "Mostrar un aviso únicamente a las visitas de ZeroNet"
author = ["drymer"]
date = 2016-08-22T16:40:00+02:00
tags = ["zeronet"]
draft = false
url = "/posts/mostrar-un-aviso-unicamente-a-las-visitas-de-zeronet/"
+++

Ya comenté en el [primer articulo](/posts/como-replicar-una-web-de-html-estatico-en-zeronet/) sobre ZeroNet que hablaría de como mostrar avisos sólo a las visitas de ZeroNet, así que ahí va. Mi motivo para querer usar esto es que en la versión de la web de ZeroNet no se puede comentar, por lo que quería que se avisase sólo a ellas.

Para ello modifiqué un poco el script que se da en el articulo de como [crear una web en ZeroNet](http://127.0.0.1:43110/Blog.ZeroNetwork.bit/?Post:43:ZeroNet+site+development+tutorial+1). Probablemente se pueda recortar aún más, más pero no sé javaScript. Lo que hace el script es establecer una conexión WebSocket y si eso pasa sale un aviso en una cajita azul. Esta conexión sólo funciona si la web se visita desde ZeroNet. Cuando se visita desde cualquier otro sitio, no pasa nada de nada. El script se puede descargar de [aquí](https://daemons.cf/wp-content/all.js) Si se quiere modificar el mensaje que sale, hay que modificar la linea 139.  El CSS que uso es:

```css
.alert-box {
  color: #555;
  border-radius: 10px;
  font-family: Tahoma,Geneva,Arial,sans-serif;font-size:11px;
  padding: 10px 10px 10px 36px;
  margin: 10px;
}

.notice {
  border: 1px solid #8ed9f6;
  background: #e3f7fc 10px 50%;
}
```

Y por último, en los archivos HTML en los que se quiere que aparezca el aviso, hay que insertar lo siguiente:

```html
...
<head>
...
<script type="text/javascript" src="$RUTA_A_SCRIPT/all.s" async></script>
...
</head>
<body>
<ul id="messages"></ul>
...
</body>
```

La parte del `script`, carga el script que se puede descargar más arriba y la parte del `ul` es dónde se inserta el aviso en cuestión.
