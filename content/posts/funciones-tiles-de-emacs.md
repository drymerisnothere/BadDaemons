+++
title = "Funciones básicas de emacs"
author = ["drymer"]
date = 2016-09-20T08:45:00+02:00
tags = ["emacs"]
draft = false
+++

A medida que he ido usando Ivy a lo largo de esta semana he visto algunas funciones que vale la pena mencionar aunque no son propias de Ivy cómo tal. Todas las que mencionaré están tanto nativamente cómo en Helm. Hay que recordar que en realidad Ivy no es más que un frontend, se puede poner por delante de cualquier función. Estas funciones les irán especialmente bien a quien no tenga demasiada experiencia en emacs:

```emacs-lisp
;; Desactiva los atajos que usaremos
(global-unset-key (kbd "C-h f"))
(global-unset-key (kbd "C-h v"))
(global-unset-key (kbd "C-h b"))
(global-unset-key (kbd "C-c b"))
(global-unset-key (kbd "C-x 4 b"))
;; Sustituye describe-function
(global-set-key (kbd "C-h f") 'counsel-describe-function)
;; Sustituye describe-variable
(global-set-key (kbd "C-h v") 'counsel-describe-variable)
;; Sustituye descbinds
(global-set-key (kbd "C-h b") 'counsel-descbinds)
;; Sustituye imenu
(global-set-key (kbd "C-c b") 'counsel-imenu)
;; Sustituye switch-buffer-other-window
(global-set-key (kbd "C-x 4 b") 'ivy-switch-buffer-other-window)
```

Estas son funciones a las cuales Ivy ofrece un frontend. Que hacen estas funciones?

-   **describe-function**: Describe funciones internas de emacs y muestra su atajo de teclado, si lo tiene.

-   **describe-variable**: Describe variables internas de emacs y muestra su valor actual.

-   **descbinds**: Describe los atajos de teclados asociados a los modos activos.

-   **imenu**: Salta a distintos sitios del buffer, sobretodo en función de las cabeceras. En buffers de org-mode salta por los nodos, en un buffer de python salta por las funciones.

-   **ivy-switch-buffer-other-window**: Abre una nueva ventana con el buffer del archivo que le digamos.

Me ha sorprendido gratamente **imenu** con el frontend de **counsel** delante. Echaba en falta algo para moverme más rápidamente por un buffer relativamente grande, de más de 500 lineas.

Otra función sin frontend en Ivy (ya que no la necesita) es:

-   **describe-key**: Describe que función ejecuta un atajo de teclado
