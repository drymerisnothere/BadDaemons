+++
title = "Botón de los cojones para compartir en GNU Social"
author = ["drymer"]
date = 2015-11-05T19:34:00+01:00
tags = ["gnusocial"]
draft = false
+++

Pues eso, he visto por GNU Social en varias ocasiones la discusión sobre el botoncito, así que ahí va un pequeño tutorial.

Aviso de que hace falta tener acceso por terminal al servidor en cuestión. Funciona con cualquier tipo de web, no es un plugin de wordpress ni nada. Así que al lío. Nos metemos en el servidor por ssh y vamos al directorio en el que queremos el plugin. De ser un wordpress, tiene sentido ponerlo en wp-content/plugins. Una vez en ese directorio:

```sh
git clone https://github.com/chimo/gs-share.git
cd gs-share
# Dado que ese plugin hace mucho que no se actualiza, nos podemos cargar el .git
rm -rf .git*
```

Ahora vamos a modificar un comportamiento que tiene por defecto. Si miráis en el botón de este mismo articulo, hacia el final, veréis que hay un tick que dice **bookmark**. Ni sé que hace exactamente el bookmark en GNU Social, pero si que evita que se comparta el articulo correctamente. Y la cosa es que por defecto sale clicado. Así que vamos a modificar el plugin para que no salga clicado. Estas son las lineas de 93 a 96 del archivo s/gs-share.s

```s
frm.innerHTML = '<label for="gs-account">' + i18n.yourAcctId + '</label>' +
  '<input type="text" id="gs-account" placeholder="' + i18n.idPlaceholder + '" />' +
  '<input type="checkbox" checked id="gs-bookmark" /> <label for="gs-bookmark">' + i18n.shareAsBookmark + '</label>' +
  '<input type="submit" value=\''+ i18n.submit + '\'/>';
```

Ahora hay que quitar la palabra **checked** de la linea 95, tal que así:

```s
frm.innerHTML = '<label for="gs-account">' + i18n.yourAcctId + '</label>' +
  '<input type="text" id="gs-account" placeholder="' + i18n.idPlaceholder + '" />' +
  '<input type="checkbox" id="gs-bookmark" /> <label for="gs-bookmark">' + i18n.shareAsBookmark + '</label>' +
  '<input type="submit" value=\''+ i18n.submit + '\'/>';
```

Ahora ya estamos listos para proceder a insertarlo. Ahora tenéis que encontrar la manera de insertar cierto código de manera automática, o no. Yo es que soy muy vago. Esto dependerá bastante según el sitio web que uséis. El siguiente método ha funcionado en un wordpress (el de [Jess](http://diariodeunalinuxera.com), por cierto), pero en el mío no y tuve que usar otro método, luego explico por qué.

En cada articulo hay que meter el siguiente código, a mano. Lo suyo seria tener un plugin que lo agregue a cada uno de manera automático, pero no he encontrado ninguno.

```text
<!-- Ejemplo funcional de su blog-->
<script src="http://diariodeunalinuxera.com/wp-content/plugins/gs-share/s/gs-share.s"></script>
<div class="gs-share"><button data-url="" data-title="" class="s-gs-share">Compartir en GNU Social</button></div>
```

A mi no me ha funcionado por un motivo muy simple. El archivo script tiene que estar una sola vez por página y en mi web se carga todo el cuerpo de los articulos, por lo que ese archivo sale varias veces. Por suerte, con nikola sólo he tenido que meter en la sección del header esa primera línea, y en el script del deploy un pequeño if que compruebe cada vez si ese articulo tiene el botóncito de los cojones.

```python
# Linea 889 en mi conf.py
EXTRA_HEAD_DATA = '<link rel="stylesheet" href="http://daemons.it/gs-share/css/styles.css" />'
```

Y en el script concretado en el deploy:

```sh
for i in $(ls posts | grep .wp)
do
  filtro=$(grep "s-gs-share" posts/$i)
  if [[ -z $filtro ]]
  then
      echo '<div class="gs-share"><button data-url="" data-title="" class="s-gs-share">Compartir en GNU Social</button></div>' >> "posts/$i"
  fi
done
```

A lo cutre. Pero si funciona, que más da que sea cutre.

PD: Es tan cutre, que al hablar de ello en este articulo, ha detectado que el botón "ya estaba" y no lo ha añadido.
