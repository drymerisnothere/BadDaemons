+++
title = "Que tecla iba ahora?"
author = ["drymer"]
date = 2017-05-22T20:48:00+02:00
tags = ["emacs"]
draft = false
+++

Cuando empiezas a usar un modo de emacs lo más habitual es intentar aprender los atajos. Siempre es algo complicado, al menos para mi, que tengo la memoria de un pez. Lo que hacia hasta ahora era mantener la página del proyecto abierta en el navegador o apuntarme los binds. O simplemente presionar `M-x`, escribir la función que quiero ejecutar, ver que atajo de teclado tiene asignado (ivy te lo muestra) y presionarlo por eso de la memoria muscular. Pero hace poco descubrí **which-key**.

Lo que hace es mostrar los posibles atajos de teclado cuando se ha apretado el inicio de una combinación. Por ejemplo, si presiono `C-c` me mostrará todas las combinaciones que empiecen por `C-c`. Se verá más claro en esta imagen.

{{< figure src="/img/which-key.png" >}}

Y la configuración es muy simple:

```emacs-lisp
(use-package which-key
  :ensure t
  :config
  (setq which-key-allow-evil-operators t)
  (setq which-key-show-operator-state-maps t)
  (which-key-mode))
```
