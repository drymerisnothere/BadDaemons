+++
title = "Charla: Emacs no necesita esteroides"
author = ["drymer"]
date = 2018-04-21T22:42:00+02:00
tags = ["charlas", "emacs"]
draft = false
+++

Este miércoles 25 de abril, a las 20:00, daré una charla sobre emacs en la Brecha Digital. Veremos, a grandes rasgos, qué es exactamente emacs, sus orígenes, comparación con vim para darle emoción, que posibilidades de uso tiene y cómo empezar a usarlo.
El nivel de la charla será introductorio, pero si ya conoces emacs siempre puedes aprovechar la ocasión para ver la Brecha Digital, un grupo que se reúne en La Brecha, un centro social alquilado, en el que nos untamos habitualmente para dar charlas y aprender tecnologías molonas en común. Más información de La Brecha Digital [aquí](https://labrecha.digital), echadle un ojo para ver como llegar a La Brecha.
