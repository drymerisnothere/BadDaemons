+++
title = "Migrar de helm a ivy"
author = ["drymer"]
date = 2016-09-13T22:30:00+02:00
tags = ["ivy", "helm", "emacs"]
draft = false
+++

Ya hablé hace un tiempo sobre [helm](https://daemons.cf/posts/introduccin-a-helm/). Helm es uno de los paquetes más potentes que tiene emacs y me sigue encantando por ello. Pero tiene un pequeño problema y es que consume demasiado para mi ordenador. `ivy`, en cambio, está a medio camino de `ido` y `helm`, manteniendo los atajos más típicos de emacs. Me refiero al uso del tabulador, que seguramente es a lo que más cuesta acostumbrarse cuando se empieza a usar `helm`. Con `ivy` se recupera el estilo típico. No tiene tantas opciones de configuración cómo `helm` y tampoco es tan simple como `ido`. Es perfecto para quien quiere `helm` pero le resulta pesado.

A esta utilidad se le llama comúnmente `ivy`, pero en realidad el paquete que instalamos es un conunto de tres herramientas:

-   **Ivy**: un mecanismo genérico de completado de emacs

-   **Counsel**: varios comandos habituales de emacs mejorados con **ivy**

-   **Swiper**: un `isearch` mejorado con **ivy**

Los tres paquetes se instalaran al instalar `ivy`. Pero si queremos mejorar la experiencia, podemos instalar dos paquetes más, `smex` y `flx.` El primero da un histórico de las órdenes ejecutadas con `M-x`, el segundo da un mejor soporte a **swipe** cuando se usa (no se como traducir esto) "expresiones regulares difusas", "fuzzy regex" en inglés. Lo bueno de ambos es que no necesitan configuración de ningún tipo.

Primero de todo instalamos los paquetes:

-   `M-x package-install RET ivy RET`

-   `M-x package-install RET flx RET`

-   `M-x package-install RET smex RET`

Una vez instalado lo configuraremos, pero veréis que es muy sencillo.

```emacs-lisp
(unless (require 'ivy nil 'noerror)
  (sleep-for 5))

(use-package ivy
  :init
  ;; Añade los buffers de bookmarks y de recentf
  (setq ivy-use-virtual-buffers t)
  ;; Muestra las coincidencias con lo que se escribe y la posicion en estas
  (setq ivy-count-format "(%d/%d) ")
  ;; Un mejor buscador
  (setq ivy-re-builders-alist
	'((read-file-name-internal . ivy--regex-fuzzy)
	  (t . ivy--regex-plus)))
  ;; No se sale del minibuffer si se encuentra un error
  (setq ivy-on-del-error-function nil)
  ;; ivy mete el simbolo ^ al ejecutar algunas ordenes, así se quita
  (setq ivy-initial-inputs-alist nil)
  ;; Dar la vuelta a los candidatos
  (setq ivy-wrap t)
  ;; Ver la ruta de los ficheros virtuales
  (setq ivy-virtual-abbreviate 'full)
  ;; Seleccionar el candidato actual (C-m en vez de C-S-m)
  (setq ivy-use-selectable-prompt t)

  ;; Asegurarse de que están smex, flx y ivi-hydra
  (use-package smex :ensure t)
  (use-package flx :ensure t)
  (use-package ivy-hydra :ensure t)
  :config
  (ivy-mode 1)
  (setq magit-completing-read-function 'ivy-completing-read)
  :diminish ivy-mode
  :ensure t)

(use-package counsel
	 :config
	 (setq counsel-find-file-at-point t)
	 :ensure t)

(use-package swiper
  ;; comentado en favor de ace-isearch
  ;; :bind (;; Sustituir isearch
  ;; 	 ("C-s" . swiper)
  ;; 	 ("C-r" . swiper))
  :ensure t)
```

No hay mucho que comentar más que la última variable. Sin esa variable apuntando a `nil`, cada vez que se ejecuta `counsel-M-x` (entre otras funciones) en el minibuffer en el que escribimos se insertará el símbolo **^**. Quien sepa algo de expresiones regulares, sabrá que ese simbolo se usa para establecer que lo que se escriba a continuación será el inicio de la linea. Un ejemplo:

```sh
# No se producirán coincidencias
echo "Hola, soy drymer, que tal?" | egrep "^drymer"
# Se producirán coincidencias
echo "Hola, soy drymer, que tal?" | egrep "drymer"
```

Sin el simbolo habrán maś coincidencias, pero no hasta el punto de ser molesto.

Y ya está, es sencillo y rápido. Quien quiera saber más sobre la configuración puede echar un ojo a su [manual de usuario](http://oremacs.com/swiper/). En el arranque no se nota mucha mejora en el rendimiento, pero en el momento de usarlo si se nota.
