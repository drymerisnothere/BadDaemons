+++
title = "Cliente de GNU social para emacs"
author = ["drymer"]
date = 2016-10-04T08:30:00+02:00
tags = ["emacs", "gnusocial"]
draft = false
+++

En un [articulo](https://daemons.cf/posts/gnu-social-en-emacs/) de [@maxxcan](https://gnusocial.net/maxxcan) ya vimos como usar `identica-mode` como cliente de emacs. Como su propio nombre indica, está pensado para el entonces centralizado servicio que era Statusnet, cuyo servidor principal era identi.ca. Sin embargo [@bob](https://primatemind.info/bob), alias [@bashrc](https://github.com/bashrc/gnu-social-mode), lo ha actualizado, cambiando nombres de funciones y creando otras.

Así que procedemos a instalarlo. De momento está en github. igual en un tiempo se anima a meterlo en Elpa.

```sh
mkdir -p ~/.emacs.d/lisp/
git clone https://github.com/bashrc/gnu-social-mode ~/.emacs.d/lisp/gnu-social-mode/
```

Ahora lo configuramos. Añadimos el directorio a la ruta de ejecución y luego establecemos las variables mínimas.

```emacs-lisp
(add-to-list 'el-get-sources '(:name gnu-social-mode
                                     :description "gnu-social client"
                                     :type github
                                     :pkgname "bashrc/gnu-social-mode"))

(if (not (el-get-package-installed-p 'gnu-social-mode))
    (el-get 'sync 'gnu-social-mode))

(use-package gnu-social-mode
  :load-path "el-get/gnu-social-mode/"
  :init
  (setq gnu-social-server-textlimit 140
	gnu-social-server "quitter.se"
	gnu-social-username "drymer"
	gnu-social-password "lolazing"
	gnu-social-new-dents-count 1
	gnu-social-status-format "%i %s,  %@:\n  %h%t\n\n"
	gnu-social-statuses-count 200))
```

La primera variable, `gnu-social-server-textlimit`, establece el máximo de caracteres que nos permitirá enviar emacs. Debería ser el mismo que el que usa el servidor, en mi caso uso [https://quitter.se/](https://quitter.se/) así que son 140. Las variables `*-server`, `*-username` y `*-password` son obvias. La de `gnu-social-status-format` estable el formato de las noticias. Sin entrar en detalles, así queda mejor que como viene por defecto. Si queréis saber más, `C-h v gnu-social-status-format RET`. Y la última, `gnu-social-statuses-count`. Son las noticias que mostrará cada vez que carguemos una linea temporal. Por defecto son 20, lo que a mi me parece poco.

Gestionar GS desde emacs es bastante sencillo, a continuación una tabla con las funciones más habituales con atajos de teclado pre-establecidos.

| Atajo de teclado | Función                                        |
|------------------|------------------------------------------------|
| / k              | Moverse una noticia arriba / abajo             |
| A                | Responder a todas                              |
| r                | Repostear                                      |
| F                | Añadir a favorito                              |
| R                | Responder a la persona que postea              |
| C-c C-s          | Publicar una noticia                           |
| C-c C-r          | Ir a la linea temporal de respuestas           |
| C-c C-f          | Ir a la linea temporal de amigos (por defecto) |
| C-c C-v          | Ir a tu perfil                                 |
| C-c C-a          | Ir a linea temporal pública                    |
| C-c C-g          | Ir a linea temporal de grupo                   |
| C-c C-u          | Ir a linea temporal propia                     |
| C-c C-o          | Ir a linea temporal de usuario concreto        |

Recordemos que estos atajos son opcionales. Siempre podemos ejecutarlos usando M-x.

Aquí una captura de cómo se ve.

{{< figure src="/img/2016-10-03-112112_1024x768_scrot.png" >}}
