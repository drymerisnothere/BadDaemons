+++
title = "Hacer la configuración de emacs auto-instalable"
author = ["drymer"]
date = 2016-11-15T08:30:00+01:00
tags = ["emacs"]
draft = false
+++

Con el-get, use-package y un poco de elisp esto se hace fácilmente. Lo hice con la mía más por hacer el experimento que por utilidad, tampoco es que tenga un ordenador nuevo por configurar a menudo. Pero esto me ha servido para tener la misma configuración de forma sencilla tanto en mi ordenador del trabajo (que por cierto, tengo con winsux y cygwin) y el de mi casa.

Para quien no haya leído los anteriores articulos sobre el-get y use-package, puede verlos [aquí](https://daemons.cf/posts/el-get-otro-instalador-de-paquetes) y [aquí](https://daemons.cf/posts/use-package-aislar-la-configuracin-de-cada-paquete), respectivamente.

use-package lo pone muy fácil para auto-instalar los paquetes que no tengas. Solo hay que establecer la propiedad **:ensure** en **t**, tal que así:

```emacs-lisp
(use-package pep8
:ensure t)
```

En el caso de el-get habrá que usar un poco de elisp, pero es muy simple, por suerte. Se usa la función `el-get-package-installed-p` (homónima de `package-installed-p` de `package.el`). Poniendo un ejemplo visto en el articulo de el-get:

```emacs-lisp
;; Definimos la fuente del paquete como ya se vio
(add-to-list 'el-get-sources '(:name xlicense-github
				       :type github
				       :pkgname "timberman/xlicense-el"
				       ))

;; Si no está instalado xlicense-github mediante el-get, se instala
(if (not (el-get-package-installed-p 'xlicense-github))
(el-get 'sync 'xlicense-github)
)

```

Y ale, a volar. Ahora, con tener la configuración complementada con lo comentado arriba y ejecutar emacs, instalaremos todos los paquetes. Para hacer la prueba, recomiendo mover el directorio `~/.emacs.d/` y recrearlo vacío únicamente con la configuración, sin los directorios `~/.emacs.d/el-get/` ni `~/.emacs.d/elpa/`. Para ver más ejemplos, se puede ver [mi configuración](https://daemons.cf/stories/mi-configuracin-de-emacs/).
