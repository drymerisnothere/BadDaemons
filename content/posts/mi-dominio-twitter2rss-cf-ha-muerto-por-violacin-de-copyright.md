+++
title = "Mi dominio twitter2rss.cf ha muerto por violación de copyright"
author = ["drymer"]
date = 2016-08-14T00:00:00+02:00
draft = false
+++

El programa que hice para crear RSS de cuentas twitter está hosteado en un servidor y tenia el dominio mencionado, twitter2rss.cf. Pero parece que ha sido denunciado por violar el copyright, por algún motivo. Imagino que por tener twitter en el nombre, aunque no es ni mucho menos el único y ni siquiera el más conocido. He enviado un correo al proveedor, freenom, pero no han contestado.

En fin, solo informar de eso. El servicio sigue siendo accesible usando la IP, no voy a ponerle otro dominio, prefiero no arriesgarme a perder el del blog. La IP es 185.101.93.139.
