+++
title = "Ejecución asíncrona de bloques babel en org-mode"
author = ["drymer"]
date = 2017-05-28T11:23:00+02:00
tags = ["emacs"]
draft = false
url = "/posts/ejecucion-asincrona-de-bloques-babel-en-org-mode/"
+++

En el articulo sobre la [programación literaria para sysadmins](https://daemons.it/posts/programacion-literaria-para-sysadminsdevops/) se pudo ver que los bloques de babel en org-mode son muy útiles. Lo malo que tiene ejecutar estos bloques es que se bloquea emacs. El dichoso tema de la concurrencia de nueva. Pero como siempre, se puede trampear.

Se puede abrir un proceso paralelo de emacs para que ejecute el bloque sin que bloquee el proceso de emacs principal. Me estaba pegando con ello justo cuando descubrí que alguien ya lo habia hecho mejor. Veamos un ejemplo. Primero un ejemplo síncrono, que no tiene mucho sentido pero es gratis:

<a id="org3b6e1ac"></a>

{{< figure src="/img/no-async-babel.gif" >}}

<a id="org24a915c"></a>

{{< figure src="/img/async-babel.gif" >}}

Muy fácil de usar, como se puede ver. Lo único malo que tiene es que no se puede usar la variable `:session`, por lo tanto se pierde la idempotencia. Se puede usar `:noweb`, pero cada bloque ejecutará todo lo anterior, lógicamente. Pero bueno, no me suele afectar demasiado para lo que uso. Para instalar el paquete que habilita este parámetro hay que añadir esto al archivo de configuración de emacs:

```emacs-lisp
(add-to-list 'el-get-sources '(:name ob-async
                                    :type github
                                    :pkgname "astahlman/ob-async"))

(if (not (el-get-package-installed-p 'ob-async))
(el-get 'sync 'ob-async))

(use-package ob-async
:load-path "el-get/ob-async/")
```

Una vez instalado y configurado, se puede usar como cualquier otro parámetro de bloque. Se puede establecer en el propio bloque, como se puede ver en el segundo gif, o se puede establecer como una propiedad en un árbol.
