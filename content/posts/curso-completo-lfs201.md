+++
title = "Curso completo LFS201"
author = ["drymer"]
draft = true
+++

Durante el año anterior estuve publicando en GNU social los apuntes que iba haciendo del curso [Essentials of System Administration](https://training.linuxfoundation.org/linux-courses/system-administration-training/essentials-of-system-administration) de la Linux Foundation. En un principio eso iba a ser todo, mis apuntes, por que copiar todo el curso iba a ser complicado. Pero hubo suerte, por haber publicado los apuntes y alguna queja sobre el curso (se puede ver en el [README](https://daemons.cf/cgit/apuntes-LFS201/about/) del repositorio git) me contactó una persona con la que he ido manteniendo el contacto, que me confirmó que el curso era bastante mierdoso y que ella había conseguido sacar el código HTML de este. Esta persona prefiere quedar en el anonimato, pero a quien le sirva el curso que le mande muchos besitos imaginarios.

Al final he desde mis apuntes, ya que pueden servir de algo a alguien, pero el curso es la fuente oficial, así que vosotros veréis. Hay varias cosas del examen que deberian saberse, recomiendo leer el README detenidamente y sobretodo la página de la certificación.

Este curso te prepara para ser un sysadmin del ñú, al examinarse hay que escoger entre Ubuntu, openSUSE y Red Hat. El curso varia en precio, yo lo cogí por unos 200€ y ahora está en 400€. Lo que si es cierto es que suele haber al menos 200€ entre el precio del curso y el del examen solamente.

Se puede acceder de distintos modos al curso:

-   Clonar el repositorio

    ```sh
    git clone https://daemons.it/drymer/apuntes-LFS201/
    ```

-   Verlo online
    -   [Apuntes HTML](https://daemons.cf/LFS201/apuntes.html)

    -   [Apuntes PDF](https://daemons.cf/LFS201/apuntes.pdf)

    -   [Curso HTML](https://daemons.cf/LFS201/curso-completo.html)

Probablemente haya alguna falta o esté mal formateado, de ser así agradeceré un correo o un comentario.
