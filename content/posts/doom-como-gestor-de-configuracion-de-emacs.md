+++
title = "Doom: un framework para configurar emacs"
author = ["drymer"]
date = 2019-02-04T22:11:00+01:00
tags = ["emacs", "doom"]
draft = false
+++

Emacs es seguramente la tecnología a la que más tiempo de estudio le he dedicado (y dedico) desde que empezó mi interés por los ordenadores. Y si algo he sacado en claro es que, dado mis otros intereses, nunca le dedicaré el tiempo a mi configuración personal que se merece. Lo he hecho durante mucho tiempo y lo seguiré haciendo, pero he decidido que no lo haré "a pelo", sino que usaré ciertas facilidades que da doom.

Qué es doom? En realidad es la configuración de emacs de [hlissner](https://github.com/hlissner/). Este humano quería usar emacs con los atajos de vim y quería que emacs se pareciese a un IDE moderno. Además, decidió que quería que la gestión de este fuese mediante la terminal.

Las características principales que tiene son:

-   Un `Makefile` para gestionar la instalación de paquetes, actualización y otros
-   Gestión de paquetes declarativa mediante `def-package`, un wrapper para `use-package` y `quelpa`
-   Un gestor de popups mediante `shackle`
-   Atajos de teclado de vim con `evil-mode`
-   Completado de código mediante `company-mode`
-   Gestión de proyectos mediante `projectile`
-   Workspaces mediante `persp-mode`
-   Es bonito
-   Arranca rápido (a día de hoy tengo 300 paquetes y ha tardado 4 segundos en arrancar)

Quien ya use estos programas, puede que piense que es una tontería, que no aporta nada. Pero la principal característica, que no mencionan en la página del proyecto es que alrededor de todos estos paquetes mencionados, hay wrappers que hacen que se integren entre ellos. Veremos más adelante a que me refiero.

La característica que más me gusto fue la de workspaces. Ahora solo tengo que ejecutar `projectile-switch-project` y el proyecto que abra, se abrirá en un workspace nuevo, sin que los buffers que abra se mezclen con los que ya tengo abiertos. Un video vale más que mil palabras.

<video width="720" height="540" controls loop align="center">
  <source src="/videos/demo-doom.ogv" type="video/ogg">
Your browser does not support the video tag.
</video>

En este video se puede ver que en el primer `workspace`, abro el proyecto de mi página web. En el segundo, en cambio, abro uno gestión de infraestructura con ansible. Y ambos están separados, cuando en cada uno de ellos ejecuto `find-file`, me muestra solo los ficheros de ese proyecto

Para probarlo rápidamente, se puede ejecutar lo siguiente:

```bash
git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
cd ~/.emacs.d
cp init.example.el init.el
make install
emacs
```

En adelante mi configuración estará en [este repositorio](https://git.daemons.it/drymer/doom-emacs-dotfiles). Dado que hay muchos cambios respecto a como lo tenia antes, prefiero crear un repositorio nuevo y mantener el [antiguo como referencia](https://git.daemons.it/drymer/emacs-dotfiles).
