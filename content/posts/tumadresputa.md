+++
title = "Van dos y tu madre es puta"
author = ["drymer"]
date = 2015-06-16T19:15:00+02:00
draft = false
tags = ["elbinario"]
+++
**Actualización**: Desde que escribí este articulo, mi opinión sobre el tema ha cambiado. Aún así, no me parece correcto borrarlo, así que aquí se queda.

Se ha hablado mucho ultimamente del tal Zapata, hasta verse forzado a la dimisión. Mi opinión al respecto es la siguiente:

-   ¿Cómo duerme Irene Villa?
-   A pierna suelta.

-   ¿Que hacen dos epilepticos en una discoteca?
-   La fiesta de la espuma.

-   ¿Que tiene dos patas y sangra mucho?
-   Medio perro.

-   ¿Que es más gracioso que un bebé muerto?
-   Un bebé muerto vestido de payaso.

-   ¿Por que en Africa no ven Bola de Dragón?
-   Por que lo echan después de comer.

-   Papá, ¿que voy a ser de mayor?.
-   Nada hijo, tienes leucemia.

-   Papá, ¿por qué cuando me agacho veo cuatro huevos?.
-   Porque te estoy dando por culo, hijo.

Creo que no me he dejado ninguna minoria, pero igual se me ha ido alguna, favor comentarlo y vemos que se puede hacer.

{{< figure src="/img/wpid-d9c75-moskimoj.jpg" >}}

PD: Si te sientes ofendido por alguno de los chistes anteriores y sientes la necesidad de compartirlo con el mundo, te pido perdón. Estoy seguro de que debe ser muy duro **no saber** cómo gestionar **tus sentimientos**. Mucho ánimo.

