+++
title = "GNU social en Emacs"
author = ["drymer"]
date = 2016-03-17T00:06:00+01:00
tags = ["emacs", "gnusocial"]
draft = false
+++

Los que somos usuarios de **Emacs** solemos estar encantados de usar nuestro programa preferido siempre que podemos. La comodidad y la tranquilidad de estar en un sistema completo y muy bien integrado es uno de los motivos de ese bienestar aunque hay muchos más y cada uno además tiene los suyos propios.

Para los que no lo sepan Emacs es mucho más que un editor o un IDE de programación, es un **intérprete** de elisp (Emacs lisp), por lo que ejecuta todo tipo de programas realizados en este lenguaje. Y os puedo asegurar que gracias a su **gestor de software** podemos acceder a muchos programas e instalarlos fácilmente. Así, y como una opción más podemos tener dentro de nuestro Emacs, clientes de correo, reproductores de música, terminales de comandos, clientes de mensajería y un largo etcétera.

Hoy voy a explicar como podemos acceder en nuestro programa preferido a **gnusocial**.

Para ello vamos a instalar **identica-mode** , un modo mayor para la red social **identica**, pero que nos vale para cualquier red status.net

Para instalar este modo lo podemos descargar de [aquí](http://blog.gabrielsaldana.org/u/1), aunque yo siempre recomendaré usar el gestor de software que incorpora Emacs.

Una vez instalado tendremos que añadir lo siguiente a nuestro fichero de inicio, por ejemplo, .emacs:

```elisp
(require 'identica-mode)
(setq statusnet-server "gnusocial.net")
(setq identica-username "tu-usuario")
(setq identica-password "tu-password")
```

Una vez hecho, teclados M-x y tecleamos identica-mode y ya podemos ver nuestro timeline de gnusocial en nuestro querido Emacs.

Más información en [el blog de su autor](http://blog.gabrielsaldana.org/identica-mode-for-emacs/).
