+++
title = "Arreglar completado de texto en elpy-doc"
author = ["drymer"]
date = 2017-02-28T08:30:00+01:00
tags = ["ivy", "python", "elpy", "helm"]
draft = false
+++

Quien leyese el articulo de como [convertir emacs en un IDE de python](https://daemons.cf/posts/programando-en-python-en-emacs/), tal vez se fió en lo útil de `elpy-doc`. Es una orden muy útil, pero usada con el auto-completado que ofrece [ivy](https://daemons.cf/categories/ivy/), da problemas. Solo ofrece el auto-completado de la primera parte del paquete.

Es decir, si queremos ver la documentación de `datetime.datetime`, ivy solo ofrecerá hasta el primer `datetime`. Esto es debido a la forma de ofrecer completados de elpy. Sin entrar en si es correcto o no, su desarrollador [no quiere](https://github.com/jorgenschaefer/elpy/issues/1084) hacerlo compatible con ivy o helm. Por ello, el desarrollador de ivy [ha añadido](https://github.com/abo-abo/swiper/issues/892) una variable en la que se puede concretar funciones en las que no queremos que el completado funcione. Es un apaño cutre, pero dado que los desarrolladores no han podido llegar a un acuerdo, al menos podemos usar `elpy-doc` correctamente con ivy. La variable se llama `ivy-completing-read-handlers-alist` y el equivalente en helm es `helm-completing-read-handlers-alist`. Añadimos la siguiente línea (cambiando ivy por helm si se usa este) en nuestra configuración y ale, a correr.

```emacs-lisp
(add-to-list 'ivy-completing-read-handlers-alist '(elpy-doc . completing-read-default))
```
