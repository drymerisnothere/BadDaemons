+++
title = "Añadir automáticamente símbolos de marcado en org-mode"
author = ["drymer"]
date = 2016-09-27T09:33:00+02:00
tags = ["orgmode", "emacs"]
draft = false
+++

Quien use [org-mode](https://daemons.cf/categories/orgmode/) sabrá de la existencia de los símbolos de marcado. Me refiero la _cursiva_, el <span class="underline">subrayado</span>, **negrita** y el `resaltado`. Yo, como soy muy vago, he buscado la forma de que al presionar una vez cualquiera de los carácteres de marcado anteriores se inserte el segundo, del mismo modo que `electric-pair-mode` cierra los paréntesis o los corchetes.

Para ello hay que modificar la tabla de sintaxis de **org-mode**:

```emacs-lisp
(with-eval-after-load 'org
  (modify-syntax-entry ?/ "(/" org-mode-syntax-table)
  (modify-syntax-entry ?* "(*" org-mode-syntax-table)
  (modify-syntax-entry ?_ "(_" org-mode-syntax-table)
  (modify-syntax-entry ?= "(=" org-mode-syntax-table))
```

Si hacéis cómo yo, que lo primero que hacéis es cargar **org-mode**, el `with-eval-after-load` no hace falta.
