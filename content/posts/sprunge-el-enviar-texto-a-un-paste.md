+++
title = "Sprunge.el - Enviar texto a un paste"
author = ["drymer"]
date = 2017-04-19T04:16:00+02:00
tags = ["emacs"]
draft = false
+++

Hace ya un par de años cuando quiero compartir texto desde la terminal, ya sea código o un fichero de configuración, uso <https://sprunge.us>. Tengo un alias hecho:

```bash
alias paste="torify curl -F '\''sprunge=<-'\'' http://sprunge.us"
```

Esto se añade al `~/.bashrc` o al `~/.zshrc`, se ejecuta algo como `cat ~/.bashrc | paste` y ale, a volar. Y evidentemente, se puede hacer lo mismo en emacs con el paquete equivalente, `sprunge.el`. Provee dos funciones, `sprunge-buffer` y `sprunge-region`, para enviar el buffer o la región seleccionada a sprunge, respectivamente. Se instala y configura tal que así:

```emacs-lisp
(add-to-list 'el-get-sources '(:name sprunge
				     :type github
				     :pkgname "tomjakubowski/sprunge.el"))

(if (not (el-get-package-installed-p 'sprunge))
    (el-get 'sync 'sprunge))

(use-package sprunge
  :init
  (use-package request :ensure t)
  :load-path "el-get/sprunge")
```

No tiene mucho misterio, solo tiene la dependencia de `request`, que está incluido en la sección de `:init`.
