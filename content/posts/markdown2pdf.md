+++
title = "Pandoc: el convertidor de archivos definitivo "
author = ["drymer"]
date = 2015-03-26T19:15:00+02:00
draft = false
tags = ["sysadmin", "elbinario"]
+++
Me he encontrado con que tengo que escribir habitualmente largos archivos a los que tengo que dar formato. Llegado el momento, y empezando como estoy con #emacs, he decidido dejar de tirar de procesoadores de texto absurdos (tales cómo *office) y tirar de emacs, que para eso está. Además, usando #markdown, que teniendo en cuenta que no necesito tablas, gráficos ni esas cosas, pues me va genial.

En fin, la cosa es que buscando cómo pasar markdown a PDF he dado con pandoc. Para que os hagáis una idea del poder de esta herramienta, ved la siguiente imagen.

{{< figure src="http://pandoc.org/diagram.png" >}}

En fin, al lío. La cosa es bastante sencilla. El paquete se llama *pandoc*, y se instala así en debian y derivados:

```bash
su -c "aptitude install pandoc"
```

En slackware está en los repositorios de slackbuilds, y tiene unas dependencias importantes. Usando sbopkg, podeis coger la lista que está en el pad, guardarla cómo/var/lib/sbopkg/queues/ pandoc.sqf y cargarla en sbopkg sin más.

Una vez instalado, para exportar un archivo markdown a PDF es tan sencillo cómo:

```bash
pandoc -o out.pdf archivo.markdown
```

Esta línea exporta el markdown tal cual. Luego es cuestión de ir metiendo opciones que nos interesen. Lo mejor que se puede hacer es documentarse en condiciones. Lo realmente interesante de pandoc es la moduralidad que permite. La exportación puede ser tan sencilla cómo hacerla tal cual está arriba o metiéndole css, templates de latexo meter metadatos al exportar a epub. El límite está dónde tu lo pongas, ya que permite incluso crear formatos en lua.
