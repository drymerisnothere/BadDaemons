+++
title = "Usar ZeroNet desde un ordenador remoto"
author = ["drymer"]
date = 2016-08-25T01:58:00+02:00
tags = ["zeronet", "bash"]
draft = false
+++

Como pasa a menudo, no hay una sola solución a un problema. Raito encontró la [siguiente](http://127.0.0.1:43110/raito.bit/?Post:12:ZeroNet+remoto), que implica usar una contraseña para acceder al ZeroNet remoto, nginx y algo más. Para mi tiene un problema y es que la mayoría de enlaces de ZeroNet que te puedan pasar estarán apuntando a la IP local, es decir, 127.0.0.1.

Por ello yo uso otra solución, que es simplemente usar `socat` desde el cliente. Para quien no lo sepa, `socat` lo que hace es redirigir puertos. Por lo tanto, en el servidor se encenderá ZeroNet de la siguiente forma en el ordenador remoto:

```sh
python zeronet.py --ui_ip $ip --tor always
```

Siendo `$ip` la IP de la máquina remota. En el ordenador cliente, se ejecutará `socat` de la siguiente manera:

```sh
socat TCP-LISTEN:43110,fork TCP:$ip:43110
```

Para mayor comodidad, he hecho un script que he colocado en `/usr/local/bin/zeronet`:

```sh
#!/bin/bash

if [[ -z $1 ]]
then
  echo "start para enchufar, stop para parar"
  exit
fi

if [[ $1 = "start" ]]
then
  socat TCP-LISTEN:43110,fork TCP:192.168.1.92:43110 &
elif [[ $1 = "stop"  ]]
then
  killall socat
fi

```

El script acepta dos argumentos, `start` y `stop`. Lo primero que hace es comprobar si se le ha pasado alguno y salir si no es así. Después, si se pasa `start`, se ejecuta socat. Si se pasa `stop`, se ejecuta un `killall`. Habría que cambiar el `stop` si se suele ejecutar `socat` para otras cosas que no sean este caso, ya que no se cargará sólo la conexión de ZeroNet, sino todas.

Y a volar.
