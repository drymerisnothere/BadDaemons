+++
title = "use-package: Aislar la configuración de cada paquete"
author = ["drymer"]
date = 2016-10-17T21:08:00+02:00
tags = ["emacs"]
draft = false
+++

Llega el momento en el que en `emacs`, como buen sistema operativo que es, te das cuenta de que tienes muchos paquetes. Y si no te organizas mínimamente, acabas con la configuración dispersa por todo el fichero. Para evitar esto se puede usar `org-babel`, como se puede ver en [mi configuración](https://daemons.cf/stories/mi-configuracin-de-emacs/). Aún así se puede rizar el rizo con `use-package`, que además de ayudar a aunar la configuración hace que se configure más rápido, ya que compila todos los archivos lisp que puede. Se supone que lo hace automáticamente, pero por lo que he visto, usando `org-babel` no se compila, así que igual hay que usarlo a pelo. Tiene más cosas buenas, pero eso lo veremos en otro articulo.

En cualquier caso sigue viniendo bien para obligar a agrupar la configuración de cada paquete. Como siempre, se instala el paquete mediante `package.el` con `M-x package-install RET use-package`. Un ejemplo, la configuración de `ivy` que ya vimos en [este articulo](https://daemons.cf/posts/migrar-de-helm-a-ivy/) pero usando `use-package`:

```lisp
(use-package ivy
:init
;; Añade los buffers de bookmarks y de recentf
(setq ivy-use-virtual-buffers t)
;; Muestra las coincidencias con lo que se escribe y la posicion en estas
(setq ivy-count-format "(%d/%d) ")
;; Un mejor buscador
(setq ivy-re-builders-alist
    '((read-file-name-internal . ivy--regex-fuzzy)
      (t . ivy--regex-plus)))
;; No se sale del minibuffer si se encuentra un error
(setq ivy-on-del-error-function nil)
;; ivy mete el simbolo ^ al ejecutar algunas ordenes, así se quita
(setq ivy-initial-inputs-alist nil)
;; Dar la vuelta a los candidatos
(setq ivy-wrap t)
;; Ver la ruta de los ficheros virtuales
(setq ivy-virtual-abbreviate 'full)
:config
(ivy-mode 1)
:diminish ivy-mode
)

(use-package counsel
:bind (
     ;; Sustituir M-x
     ("M-x" . counsel-M-x)
     ;; Sustituir imenu
     ("C-c b" . counsel-imenu)
     ;; Sustituir find-file
     ("C-x C-f" . counsel-find-file)
     ;; Sustituir describe-function
     ("C-h f" . counsel-describe-function)
     ;; Sustituir describe-variable
     ("C-h v" . counsel-describe-variable)
     ;; Sustituir descbinds
     ("C-h b" . counsel-descbinds)
     ;; Sustituye helm-show-kill-ring
     ("M-y" . counsel-yank-pop)
     )
:config
;; usar ffap
(setq counsel-find-file-at-point t)
)

(use-package swiper
:bind (
     ;; Sustituir isearch
     ("C-s" . swiper)
     ("C-r" . swiper)
     )
)
```

Hay tres parámetros que se usan habitualmente. `:bind`, `:config` y `:init`. El primero es bastante obvio, sirve para concretar los atajos de teclado a usar. El segundo sirve para ejecutar funciones o establecer variables una vez el paquete se ha cargado. Y el tercero ejecuta funciones o establece variables antes de cargar el modo. En general, las variables propias de un paquete se pueden establecer antes, pero otras veces no. Yo prefiero establecerlas antes, pero si se hace después no debería haber problema. Como digo, depende del paquete. El último parámetro, `:diminish`, no es algo necesario, pero sirve para quitar palabras innecesarias del powerline. En el caso de `ivy`, cuando está activo sale `Ivy`. Como es algo que siempre está activo, no sirve de nada que lo muestre, así que concretando el nombre del modo en esa variable, haremos que no aparezca más.

Hasta aquí las variables que más se usan. Para más información, se puede leer el [README del proyecto](https://github.com/jwiegley/use-package) o mirar [mi configuración](https://daemons.cf/stories/mi-configuracin-de-emacs/). Adelanto que en el próximo articulo veremos como automatizar la instalación de paquetes y hacer que con clonar un repositorio y encender emacs, tengamos toda nuestra configuración.

PD: Mi configuración puede que esté algo caótica a día de hoy ya que ando probando el tema de la automatización de la instalación de paquetes. En general no debería haber problema, pero si se ve algo raro, es por eso.
