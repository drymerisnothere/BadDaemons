+++
title = "Como crear subdominios en los servicios ocultos de Tor"
author = ["drymer"]
date = 2016-11-03T08:30:00+01:00
tags = ["tor"]
draft = false
+++

Es estúpidamente sencillo. No sé cuanto tiempo lleva esta opción activa, por que no la he visto anunciada en ningún sitio, simplemente he visto que que la gente lo ha empezado a usar sin más. El tema de los subdominios curiosamente no lo gestiona Tor como tal, sino los virtual hosts de los servicios. Si aún no sabéis como configurar un servicio oculto de Tor, podéis leer [este articulo](https://daemons.cf/posts/crear-un-hidden-service-con-tor/).

Un ejemplo tonto, redirigir el subdominio www de daemon4idu2oig6.onion al propio daemon4idu2oig6.onion.

```nginx
server {
listen 127.0.0.1:80;
server_name  www.daemon4idu2oig6.onion;
return 301 http://daemon4idu2oig6.onion$request_uri;
}
```

Esto ya debería pasar si se han tomado las precauciones mínimas que es establecer un default\_server en el virtual\_host general, pero bueno. En el caso de prosody, para añadir salas de chat:

```lua
Component "salas.daemon4idu2oig6.onion" "muc"
name = "Salas de Bad Daemons"
```

Y ale, a correr. Según parece, todos los subdominios apuntan a donde apunte el tld principal, si el servidor sabe gestionar ese nombre, lo hará.
