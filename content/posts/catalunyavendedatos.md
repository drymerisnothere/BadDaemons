+++
title = "Catalunya: los datos de los pacientes en manos de terceros"
author = ["drymer"]
date = 2015-04-04T19:15:00+02:00
draft = false
tags = ["privacidad", "elbinario"]
+++

DISCLAIMER: Tocho-post incoming.

En España se lleva el amarillo y el rojo. Algunas personas lo adornan con un aguilucho de estos ilegales. Otras personas le meten una estrella con cuatro rallitas. También se comenta que esta es ilegal. Pero los adornos que se le pongan a una bandera no son importante, sigue siendo una **puta** bandera.

Mientras el partido gobernante de esta comunidad autonómica sigue pasándose por el forro la legalidad de sus acciones, tales cómo el [ab]uso de la fuerza en el caso de los Mosssos, hay gente que aún piensa que nada es más importante tener una bandera con más barras rojas que preocuparse por los actos de sus gobernantes. Ahora llega un nuevo(?) atropello (no sólo típico de dónde se hace la butifarra, en Madrid y otras comunidades hacen cosas similares), que no está teniendo demasiada repercusión mediática. Será por que si les toques al señor Menos te lapidan. Que hay que ver lo anti-catalan que es alguien por ser crítico.

Se ha aprobado[^1] el proyecto llamado Visc+. Les gusta mucho eso de poner nombres modernos y que atraigan a los jóvenes, se ve. La siguiente cita, que es más bien un resumen traducido de la página del Govern[^2], dice tal que así:

### Que es VISC+?
Este proyecto permitirá poner en manos de la comunidad científica información relacionada con el sistema sanitario catalan. Previamente anonimizado, claro. Todo esto con el fin de facilitar la investigación, la innovación y la evaluación en el campo de la biomedicina.

### Con que finalidad se podran usar estos datos?
"Sólo" podrán acceder por investigadores que justifiquen que su uso se corresponde a una finalidad médica. Su finalidad será analizada tanto éticamente cómo científicamente. Esto permite la mejora en el ámbito asistencial pero también en el diagnóstico.

### Que datos se investigan?
[No para de repetirse que están previamente anonimizados] Información de atención primaria, hospitalária, imagen médica digital o información sobre la receta electrónica. Entre otros. El proyecto no accederá directamente al historial médico de un paciente.

### Esta información se ha usado antes para investigar?
Si. [Pues que gilipollez de ley, si ya se podía hacer, no?] Tendrá acceso a esta incluso organizaciones con animo de lucro.

### Beneficios
* Aumentar la calidad de la asistencia recibida.
* Beneficios para los investigadores, que tendrán acceso a muchos datos.
* Beneficios del sistema sanitario, por que obtendran los resultados de los analisis.

### Cómo se garantiza la Integridad de los datos en este proyecto?
[Esta sección existe y está en blanco...]

### Será público quien accede a estos datos y que utiliza?
Si.

### Puedo excluir mis datos de este proyecto?
Si. [En vez de pedirte si pueden darlos, tienes que decir que no pueden.]

### Algun proyecto similar en el resto del mundo?
Si. En Inglaterra, Holanda, EEUU y, sorpresa, Catalunya.

### Por qué AQuAS(Agència de Qualitat i Avaluació Sanitàries de Catalunya) tira adelante este proyecto?
Por que son buena gente y quieren mejorar la investigación y los Centros de Atención Primaria. Catalunya es muy moderna porqué hace uso del Big Data.

### Cuales son nuestros derechos respecto con nuestros datos personales?
Se aplican los llamados derechos ARCO. Acceso, rectificacion, cancelación y oposición.


Qué diferencia hay con este proyecto, respecto a anteriores tales cómo SIDIAP[^3]? Técnicismos. Poco más.

Esta es la parte bonita, en la que se habla de lo beneficioso que es el Big Data y lo moderna que es Catalunya, que está muy avanzada para la edad que tiene. Sólo les falta decir que también tiene las tetas grandes. Luego vienen las cosas de las que no hablan u obvian. Por ejemplo, este proyecto ha sido aprobado por el propi Govern, sin pasar por el Parlament. Tiene relación, o directamente han participado, con diferentes empresas cuya ética es, cómo mínimo, preocupante. Un par de ellas son PWC y IMS Health. La primera, una consultora, ha llegado a proponer los cambios necesarios al ICS para que los hospitales públicos sean fácilmente absorbibles por entidades privadas. [^4]

Evidentemente, un proyecto de este tipo puede ser muy beneficioso para la sanidad pública. Lo preocupante es lo mencionado en el anterior parágrafo, entre otras cosas. Incurren en demasiadas contradicciones. Por ejemplo, en el FAQ aparece que se puede pedir que no se incluyan tus datos en las cesiones cómo única medida para contrarestrar esta apropiación. En el Diario Oficialde Catalunya dice, sin embargo *Cession a terceros de informacion personal [...] siempre que haya consentimiento previo*. Pero el tema más preocupante es el de la anonimización de estos datos. El Govern no es conocido por sus grandes dotes en ámbitos informáticos. Su web da bastante asco y está llena de links rotos. Si un organismo cómo el CESICAT, que se dedican a la seguridad y uso de nuevas tecnologias, la cagó cómo la cagó[^5], qué nos garantiza que no lo hagan tres veces peor los de AQuAS?

Porqué, de hecho, ya se tiene en cuenta la posibilidad de des-anonimizar estos datos. En el anexo 2 de la resolución SLT/570/2015, el punto 8, habla de algo interesante. *Reidentificación de los datos anonimizados en caso de peligro*: Esto no es malo en si, pueden salvar a alguien. Lo preocupante es que realmente se puede reidentificar a alguien. Entonces, que tipo de anonimización de datos es esta? Será que con un tipo concreto de información sobre una persona, se le puede identificar fácilmente? De ser así, en que situación nos encontrariamos si las empresas que pueden contratarnos saben nuestro historial? Por no hablar de lo que podrían hacer las aseguradoras. De esto último, se hizo un documental muy interesante hace años, de Michael Moore. Recomiendo su visionado o incluso re-visionado. Las cosas se ven un poco distintas con el tiempo. Sobretodo sabiendo que algunas personas quieren dirigirnos al tipo de Sanidad que muestran en ese documental.

Cómo mencionaba arriba, aquí si se crítica al Govern es que eres anti-catalan. Entre eso y las presiones que probablemente hacen distintas empresas junto con el Govern a los medios de comunicación, no se está tratando este tema. En el Parlament poco pueden hacer (los pocos que quieren), ya que les han pasado por encima. Sólo queda la concienciación y la acción ciutadana.

Y si te piensas que por estar fuera de Catalunya esto no te afecta, estás equivocada. En el caso que esto tire adelante y empiecen a sacar pasta, crees que el resto del país no querrá sacar tajada también?



[^1]: [Fuente][1]
[^2]: [Fuente][2]
[^3]: Sidiap es un proyecto que ya existe desde el 2010 en Catalunya. Desde entonces ya tenia acceso a estos datos. Empezó cómo XIIAP (Xarxa Informatizada d'Investigació a l'AP) en el 2000, hasta 2007 que pasó a ser UNIDAP (Unitat de Dades Informatitzades de l'AP), y en 2010 pasó a ser SIDIAP (Sistema d’Informació pel Desenvolupament de la Investigació a l’Atenció Primària). Su web: [http://www.sidiap.org/](http://www.sidiap.org/)
[^4]: [Fuente][4]
[^5]: [Fuente][5]
[^6]: [Fuente][6]

[1]: http://www.20minutos.es/noticia/2421621/0/generalitat-aprueba/venta-datos-sanitarios/gestion-privada/
[2]: http://aquas.gencat.cat/ca/projectes/visc/preguntes-frequeents/#FW_bloc_dfc09e8f-85cd-11e4-8517-005056924a59_1
[3]: http://www.sidiap.org/index.php?option=com_content&view=article&id=46&Itemid=34&lang=ca
[4]: http://www.catacctsiac.cat/docs_observatori/model/empreses_publiques/20121212_document_pwc.pdf
[5]: http://www.sindicatperiodistes.cat/es/content/m%C3%A1s-filtraciones-de-seguimientos-periodistas
