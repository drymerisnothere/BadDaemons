+++
title = "Publicada la auditoria de Truecrypt"
author = ["drymer"]
date = 2015-04-03T19:15:00+02:00
draft = false
tags = ["privacidad", "elbinario"]
+++
En un bonito PDF. Lo tienen todo bastante dividido, explicando cómo han procedido con la auditoria, con qué métodos, que funciones han auditado, etc.

DISCLAIMER
A continuación escribo un pequeño resumen. Aviso que no me acerco ni de lejos a ser un experto en nada, y menos en criptografia. Por lo que perfectamente me podria equivocar en lo que interpreto del texto. Cualquier apunte o crítica es bien recibida.

Las vulnerabilidades encontradas son pocas, aunque algunas críticas. Concretamente son cuatro, dos de ellas catalogadas cómo de criticidad "alta", una "baja" y otra indeterminada.

* CryptAquireContext puede fallar silenciosamente: esta función, que parece ser la encargada de conseguir entropia en los sistemas Windows, puede fallar sin avisar. Un escenario planteado es el de una usuaria creando un Volumen Cifrado en un entorno empresarial con políticas de grupo que delimita el acceso al servicio proveedor de criptografia, provocando así que se pase a usar otros métodos más inseguros. Esto permitiria a un atacante, de conocer la fuente insegura, un ataque de fuerza bruta. Esta vulnerabilidad se cataloga cómo crítica y de dificultad de abuso indeterminada. Apunte: el proveedor de entropia es el encargado de conseguir aleatoriedad para que esta pueda ser usada por los programas que lo necesitan.

* La implementación de AES es susceptible al ataque cache-timing: AES usa tablas muy grandes, que a veces no caben en la caché de las CPU. Un atacante podría ejecutar código de manera remota para calcular el tiempo de descifrado de la clave, hacer un análisis estadístico y conseguir así la clave privada AES. Hay que hacer constar que este ataque no es propio de TrueCrypt sinó de el propio concepto de cifrado de AES. Para que un atacante lograse este ataque, tendría que tener acceso a este ordenador y la capacidad de ejecutar código nativo en la máquina. Este tipo de ataque, cómo muchos side-attacks, no se pueden erradicar. Lo único que se puede hacer es dificultar su explotación. Esta vulnerabilidad se cataloga como crítica y de dificultad de abuso alta.

* La mezcla de keyfiles no es optima: TrueCrypt coge las keyfiles y las mezcla con la contraseña del usuario tal cual, sin hashearlas. Esto, más que una vulnerabilidad se considera una mala práctica. El motivo es que al manejar diferentes fuentes de entropia, las claves en este caso, aunque un atacante controlase una no no será capaz de deducir la contraseña de desbloqueo. Lo que si podría hacer es manipular una de las claves no controladas y provocar que, al meter la contraseña, fuése cómo si no se estuviesen usando estas llaves. Esta vulnerabilidad se cataloga cómo crítica y de dificultad de abuso alta.

* Texto cifrado en las cabeceras del volumen: Hay algo de metadatos en estas cabeceras. La cabecera y el volumen están cifrados con una clave distinta. La cabecera está cifrada con la contrasñea del usuario y el volumen con una keyfile que está en la cabecera. Garantizar la integridad y la autenticidad es algo de lo que el cifrado de disco completo no pretende hacerse cargo, ya que incurriria en un rendimiento del disco más bajo. Sin embargo, este cifrado si puede tratar la integridad de la cabecera, que al fin y al cabo contiene la llave que abre el volumen. TrueCrypt intenta solucionarlo incluyendo en la cabecera un string TRUE, un CRC32 (Cyclic Redundancy Check (Comprobación Cíclica Redundante)) de la clave y otro CRC32 del resto del volumen. EL CRC32 es un método de hasheo inseguro. De no estar cifrada la cabecera, esto podría permitir el acceso a la clave maestra y por tanto al volumen cifrado.

Y esto es. Este es el análisis final de la segunda fase de la auditoria. Este es la auditoria final, aunque queda pendiente un sumario ejecutivo. Pronto saldran otra vez las teorías sobre el abandono de esta herramienta. O, más vien, se descartan algunas, tales cómo que los de TrueCrypt abandonaron por miedo a que la auditoria encontráse algo comprometido. Solo se me ocurre esa teoría en la que se vieron obligados a abandonar ante las presiones de distintos gobiernos, presumiblemente el de EEUU.

Recomiendo la lectura de la [fuente](https://opencryptoaudit.org/reports/TrueCrypt_Phase_II_NCC_OCAP_final.pdf), ya que será mucho más fiable que esto que he escrito.
